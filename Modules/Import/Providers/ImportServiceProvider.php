<?php

namespace Modules\Import\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Support\Traits\LoadsConfig;

class ImportServiceProvider extends ServiceProvider
{
    use LoadsConfig;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->loadConfigs(['permissions.php']);
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }
}
