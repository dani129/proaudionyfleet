<?php

Route::get('imports', [
    'as' => 'admin.imports.index',
    'uses' => 'ImportController@index',
    'middleware' => 'can:admin.imports.index',
]);

Route::post('imports', [
    'as' => 'admin.imports.import',
    'uses' => 'ImportController@import',
]);
