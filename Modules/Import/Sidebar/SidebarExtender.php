<?php

namespace Modules\Import\Sidebar;

use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Maatwebsite\Sidebar\Group;
use Modules\Admin\Sidebar\BaseSidebarExtender;

class SidebarExtender extends BaseSidebarExtender
{
    public function extend(Menu $menu)
    {
        $menu->group(trans('admin::sidebar.content'), function (Group $group) {
                $group->item(trans('import::imports.imports'), function (Item $item) {
                    $item->icon('fa fa-tags');
                    $item->weight(10);
                    $item->route('admin.imports.index');
                    $item->authorize(
                        $this->auth->hasAccess('admin.imports.index')
                    );
                });
           
        });
    }
}
