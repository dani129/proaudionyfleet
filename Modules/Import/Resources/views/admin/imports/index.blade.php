@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', trans('import::imports.imports'))

    <li class="active">{{ trans('import::imports.imports') }}</li>
@endcomponent

@section('content')

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                       
                            @foreach ($errors->all() as $error)
                                {{ $error }}
                            @endforeach
                        
                    </div>
                @endif
                @if(session()->has('message'))
                    <div class="alert alert-success">
                         <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        {{ session()->get('message') }}
                    </div>
                @endif
    <div class="box box-primary">
        <div class="box-body index-table" id="import-form">
            <b>Upload .CSV file</b>
            <hr>
            <form method="POST" action="{{ route('admin.imports.import') }}" class="form-horizontal" id="import-create-form" enctype="multipart/form-data" novalidate>
                {{ csrf_field() }}

                <input type="file" class="image-picker btn btn-default" name="import_file" />
                <br>
                <button class="btn btn-primary">Import File</button>
        
            </form>
        </div>
    </div>
@endsection
