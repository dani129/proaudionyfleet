<?php

return [
    'imports' => 'Imports',
    'table' => [
        'order_id' => 'Order ID',
        'transaction_id' => 'Transaction ID',
        'payment_method' => 'Payment Method',
    ],
];
