<?php

namespace Modules\Import\Http\Controllers\Admin;

use Illuminate\Routing\Controller;
use Modules\Admin\Traits\HasCrudActions;
use Modules\Import\Entities\Import;
use Modules\Media\Entities\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Modules\Media\Eloquent\HasMedia;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductTranslation;
use Modules\Category\Entities\Category;
use Modules\Category\Entities\CategoryTranslation;
use Modules\Import\Http\Requests\SaveImportRequest;
use League\Csv\Reader;

class ImportController extends Controller
{
    use HasCrudActions;
    use HasMedia;
    /**
     * Model for the resource.
     *
     * @var string
     */
    protected $model = Import::class;

    /**
     * Label of the resource.
     *
     * @var string
     */
    protected $label = 'import::imports.import';

    /**
     * View path of the resource.
     *
     * @var string
     */
    protected $viewPath = 'import::admin.imports';

    /**
     * Form requests for the resource.
     *
     * @var array
     */
    protected $validation = SaveTransactionRequest::class;

    public function import(Request $request) 
    {   
        define('DIRECTORY', public_path().'/storage/media/');
        $productArray[] = "";
       

        /*Validate file*/
        $request->validate([
            'import_file' => 'required'
        ], ['import_file.required' => 'Please choose csv file for import']);
        
        /*Read CSV*/

        if ($request->hasFile('import_file')) {
        $path = $request->file('import_file')->getPathName();
        $file = $request->file('import_file');
        $csv = Reader::createFromPath($path, 'r');
        $csv->setHeaderOffset(0);

        $input_bom = $csv->getInputBOM();

        if ($input_bom === Reader::BOM_UTF16_LE || $input_bom === Reader::BOM_UTF16_BE) {
            $csv->addStreamFilter('convert.iconv.UTF-16/UTF-8');
        }
          
          /*Store Product*/
          foreach ($csv as $key => $product) {

            $countPro = ProductTranslation::where('name',$product['Title'])->get();
                if( count($countPro) == 0 )
                  {
                
                /*Category for product */
                    if(strpos($product['Categories'], '>') !== false) {
                                
                        $categories = explode('>', $product["Categories"]);

                        $catId[] = $this->getCategory($categories);
                    
                    } elseif (!empty($product['Categories'])) {
                        # code...
                        $categories[] = $product['Categories'];
                        
                        $catId[] = $this->getCategory($categories);
                    } else {

                        $catId[] = "";
                    }

                  /*Saved and retrive images id*/
                
                 $savedImages =  $this->storeImage($product['Images']);
                 
                 if (count($savedImages) > 1)
                 {  $files = array(
                    'base_image'=> $savedImages[0],
                    "additional_images" => $savedImages
                  );
                 } else if (count($savedImages) == 1 ) {

                    $files = array(
                    'base_image'=> $savedImages[0]
                  );
                 } else {

                  $files = '';
                 }

                 /*Price handling*/
                 if(empty($product['Regular Price']))
                 {
                  $productPrice = "0.00";
                 } else {

                  $productPrice = str_replace( ',', '', $product['Regular Price']);
                 }

                 if(empty($product['Sale Price']))
                 {
                  $productSalePrice = null;
                 } else {

                  $productSalePrice = str_replace( ',', '',$product['Sale Price']);
                 }

                /*Product store*/
//                      $product['Title'] = mb_strtolower($product['Title']);
                $productArray = array (
                          "name" => $product['Title'],
                          "description" => "<p>".$product['Specs']."</p>",
                          "categories" => $catId,
                          "tax_class_id" => null,
                          "is_active" => "1",
                          "price" => $productPrice,
                          "special_price" => $productSalePrice,
                          "special_price_type" => "fixed",
                          "special_price_start" => null,
                          "special_price_end" => null,
                          "sku" => $product['Item ID'],
                          "manage_stock" => "0",
                          "qty" => null,
                          "in_stock" => "1",
                          "files" => $files,
                          "meta" => array (
                            "meta_title" => null,
                            "meta_description" => null,
                          ),
                          "attributes" => array ( array (
                              "attribute_id" => null,
                            )
                          ),
                          "options" => array (
                             array (
                              "id" => null,
                              "name" => null,
                              "type" => null,
                              "is_required" => "0",
                           )
                          ),
                          "short_description" => $product['Short Description'],
                          "new_from" => null,
                          "new_to" => null,
                        );
                $result = $this->createProduct($productArray);
            }

          }
          return redirect()->back()->with('message', 'Import Product Completed');
        }
    }

    
     /**
     * Create Category for product and return ids.
     *
     * @param array $cat
     */
    public function getCategory ($cat)
    {
        $parentId = "";

        if($cat){
          foreach ($cat as $key => $value) {
                    
            $countCat = CategoryTranslation::where('name',$cat[$key])->get();
                    
              if($key == 0){
                if( count($countCat) == 0)
                {
                            $data = array(
                                    'name' => $cat[$key],
                                    'slug' => $cat[$key],
                                    'is_searchable' => '1',
                                    'is_active' => '1'
                                );
                            $parentNew = Category::create($data); 
                            $parentId =  $parentNew->id;
                } else {

                            $parentId = $countCat[0]->category_id;
                }    
                } else {

                  if( count($countCat) == 0)
                  {
                            $data = array(
                                    'name' => $cat[$key],
                                    'slug' => $cat[$key],
                                    'is_searchable' => '1',
                                    'is_active' => '1',
                                    'parent_id' => $parentId
                                );
                            $parentNew = Category::create($data); 
                                     $parentId =  $parentNew->id;
                  } else {

                            $parentId = $countCat[0]->category_id;
                  }    
                    }
                } 
            }
                   

            return strval($parentId);
    }

     /**
     * Create Product.
     *
     * @param array $data
     */
    private function createProduct($data)
    {   
        
        $function = new Product();

        $parentNew = Product::create($data);
        $function->addCat($parentNew,$data); 
        $this->syncImportFiles($parentNew,$data['files']);           
        
    }
     /**
     * Store images reading form directory.
     *
     * @param string $folderName
     */
    public function storeImage($folderName)
    {
        $folderNameOrignal = $folderName;
//        dd($folderName);
//        $directory = public_path()."/images/".$folderName;
        $folderName = str_replace( array('/', '÷', 'Ã·', '"', ';', '<', '>', '|', ':' ), '', $folderName);
//        dd($folderName);
        $directory = "D:\Sweet Water\All Data\all/".$folderName;
        if(is_dir($directory)){
          $images = glob($directory . "/*.*");
//          dd('folder found');
//            print_r($images);exit;
        foreach($images as $file)
        {

              $pathinfo = pathinfo($file);
              $hash = hash('sha256', $file . strval(time()));
              $path = 'media/'.$hash.'.'.$pathinfo['extension'];

          if (\File::move($file , DIRECTORY.$hash.'.'.$pathinfo['extension'])) {


              $image = File::create([
                  'user_id' => auth()->id(),
                  'disk' => config('filesystems.default'),
                  'filename' => basename($file),
                  'path' => $path,
                  'extension' => $pathinfo['extension'],
                  /*'mime' => mime_content_type($file),
                  'size' => filesize($file),*/
              ]);
          }
            $productImages[] = $image->id;
        }
           return $productImages;
      } else {
        dd($folderNameOrignal." ======== not found");
        $productImages[] = '';
        return $productImages;
      } 
    }
}
