
@php
    $myArray = response()->json($product->description);
    $myArray = str_replace('<\/p>','',$myArray);
    $myArray = str_replace('<p>','',$myArray);
    $myArray = explode('\n', $myArray);
    $variable = substr($myArray[0], 0, strpos($myArray[0], '"'));
    $myArray[0] = ltrim($myArray[0],$variable);
    $myArray = str_replace('\t','&emsp;',$myArray);
    $myArray = str_replace('\r',' ',$myArray);

    $myArray = str_replace(str_split('\/"<>|'),' ',$myArray);
@endphp

<div id="description" class="description tab-pane fade in {{ request()->has('reviews') || review_form_has_error($errors) ? '' : 'active' }}">
    @foreach($myArray as $myArray)
        <p>{!! $myArray !!}</p>
    @endforeach
</div>
